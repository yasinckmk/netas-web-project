package com.netas;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.FileCopyUtils;

@Configuration
@EnableTransactionManagement
@PropertySource(value = { "classpath:/application.properties" })
@SpringBootApplication(scanBasePackages = { "com.netas" })
@EnableJpaRepositories(basePackages = {
		"com.netas" }, entityManagerFactoryRef = "netasEntityManagerFactory", transactionManagerRef = "netasTransactionManager")
public class JsfSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsfSpringBootApplication.class, args);
	}

	@Autowired
	Environment env;

	@Qualifier("netasDS")
	@Bean
	public DataSource netasDS() {

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("spring.jpa.driver.class.name"));
		dataSource.setUrl(env.getProperty("spring.datasource.url"));
		dataSource.setUsername(env.getProperty("spring.datasource.username"));
		dataSource.setPassword(env.getProperty("spring.datasource.password"));

		return dataSource;
	}

	@Qualifier("netasDBTemp")
	@Bean
	NamedParameterJdbcTemplate netasDBTemp() {
		return new NamedParameterJdbcTemplate(netasDS());
	}

	@Bean
	public PlatformTransactionManager netasTransactionManager() {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(netasEntityManagerFactory());
		return txManager;
	}

	@Bean
	public EntityManagerFactory netasEntityManagerFactory() {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");
		vendorAdapter.setGenerateDdl(true);
		vendorAdapter.setShowSql("true".equals(env.getProperty("hibernate.showsql")));

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("com.netas");
		factory.setDataSource(netasDS());

		Properties jpaProps = new Properties();
		// jpaProps.put("hibernate.current_session_context_class","thread");
		jpaProps.put("hibernate.default_schema", "Test");
		jpaProps.put("hibernate.jdbc.fetch_size", 2000);
		// Spring boot application error with connect PostgreSQL
		jpaProps.put("hibernate.jdbc.lob.non_contextual_creation", "true");
		factory.setJpaProperties(jpaProps);
		factory.afterPropertiesSet();
		return factory.getObject();
	}

	@Bean
	@Qualifier("queryUserRoleResources")
	String getQueryUserRoleResources() throws UnsupportedEncodingException, IOException {
		return new String(
				FileCopyUtils.copyToByteArray(
						this.getClass().getClassLoader().getResourceAsStream("sql/queryUserRoleResources.sql")),
				"UTF-8");

	}

}
