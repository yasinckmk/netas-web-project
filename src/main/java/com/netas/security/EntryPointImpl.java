package com.netas.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

public class EntryPointImpl implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse, AuthenticationException paramAuthenticationException) throws IOException,
			ServletException {
		paramHttpServletResponse.sendRedirect("/accessDenied.tk");
	}

}
