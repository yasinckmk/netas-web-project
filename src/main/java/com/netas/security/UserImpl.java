package com.netas.security;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

import com.netas.auth.model.Enduser;


/**
 * The user implementation of spring-security framework user class. <br>
 * Extends for our simulation of a one-time-password .<br>
 * <br>
 * Extended for usrToken, our simulation of a one-time-password .<br>
 * Extended for a usrId, userID (type long). <br>
 * Extended for SecUser. <br>
 * 
 * Sadece loakl authentication için kullanılır.
 * 
 * @author bbruhns
 * @author sgerth
 * @author ebitik
 */
public class UserImpl extends org.springframework.security.core.userdetails.User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	final private Long userId;
	private Enduser user;
	private Enduser activeManager;
	
	final private String loginTime;
	
	 private Map<String, Integer> grantCache = new HashMap<>();


	public UserImpl(Enduser user, Collection<GrantedAuthority> grantedAuthorities, boolean userStatus) throws IllegalArgumentException {
		super(null==user.getUserName()?user.getId().toString():user.getUserName()
				,null==user.getUserName()?user.getId().toString():user.getUserName()
						,userStatus,true,true,true, grantedAuthorities);
		this.userId = user.getId();
		this.user = user;
		this.loginTime = getFormattedDate(new Date(), "dd.MM.yyyy hh:mm");
		
		activeManager= user.getZper();
	}
	
	public String getFormattedDate(Date date, String pattern) {
		if (date != null) {
			return new SimpleDateFormat(pattern).format(date);
		} else {
			return "";
		}
	}

	public Long getUserId() {
		return this.userId;
	}

	public Enduser getUser() {
		return user;
	}

	public void setUser(Enduser securityUser) {
		this.user = securityUser;
	}

	public String getLoginTime() {
		return loginTime;
	}


	public Enduser getActiveManager() {
		return activeManager;
	}

	public void setActiveManager(Enduser activeManager) {
		this.activeManager = activeManager;
	}


	public Map<String, Integer> getGrantCache() {
		return grantCache;
	}

	public void setGrantCache(Map<String, Integer> grantCache) {
		this.grantCache = grantCache;
	}
		

	@Override
	public String toString() {
		return user !=null ? user.getDistinctName() : StringUtils.EMPTY;
	}
}
