package com.netas.security;

import java.util.Collection;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import org.springframework.stereotype.Service;

import com.netas.auth.model.RoleResourceInfo;



@Service("oneRoleVoter")
public class OneRoleVoter implements AccessDecisionVoter<Object> {

	public static final String IS_AUTHENTICATED_FULLY = "IS_AUTHENTICATED_FULLY";
	public static final String IS_AUTHENTICATED_REMEMBERED = "IS_AUTHENTICATED_REMEMBERED";
	public static final String IS_AUTHENTICATED_ANONYMOUSLY = "IS_AUTHENTICATED_ANONYMOUSLY";

	

	/**
	 * Hangi urle hangi rol ile baglanti saglandigini bi sekilde elde etmeliyiz.
	 * 
	 * */
	public int vote(Authentication authentication, Object object, Collection<ConfigAttribute> attributes) {
		
//		if(true)
//			return ACCESS_GRANTED;
		
		int result = ACCESS_DENIED;
		Collection<GrantedAuthority> authorities = extractAuthorities(authentication);
		UserImpl principal = (UserImpl) authentication.getPrincipal();
		
		
		FilterInvocation fi = (FilterInvocation) object;

		String url = fi.getRequestUrl();
		
		if(principal.getGrantCache().containsKey(url))
		{
			result = principal.getGrantCache().get(url);
		}else
		{
			boolean matches = matches(authorities, url);
			
			for (ConfigAttribute attribute : attributes) {
				
				if (IS_AUTHENTICATED_ANONYMOUSLY.equals(attribute.toString())) {
					result = ACCESS_GRANTED;
				}
				
				//protected sayfalar buraya gelecek, proteted lara bu rolu verebilir misin?
				if (IS_AUTHENTICATED_FULLY.equals(attribute.toString()) && matches) {
					result = ACCESS_GRANTED;
				}
		
			}
			
			principal.getGrantCache().put(url, result );
		}
	
		return result;
	}
	
	public boolean matches (Collection<GrantedAuthority> authorities, String url)
	{
		boolean result = false;
		if(authorities !=null && !authorities.isEmpty())
		{
			for(GrantedAuthority auth : authorities)
			{
				result = ((RoleResourceInfo)auth).matches(url);
				
				if(result)
					break;
			}
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	private Collection<GrantedAuthority> extractAuthorities(Authentication authentication) {
		return (Collection<GrantedAuthority>) authentication.getAuthorities();
	}
	

	@Override
	public boolean supports(ConfigAttribute attribute) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return true;
	}
}
