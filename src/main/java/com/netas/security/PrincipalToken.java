package com.netas.security;

public class PrincipalToken {
	
    private String userId;
 
  
    
	public PrincipalToken(String userId) {
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	

	@Override
	public String toString() {
		return "User ID: "+userId;
	}
		

}