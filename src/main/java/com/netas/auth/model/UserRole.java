package com.netas.auth.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.netas.db.model.AbstractModel;


/**
 * The persistent class for the FRT_USER_ROLE database table.
 * 
 */
@Entity
@Table(name = "USER_ROLE")
@NamedQuery(name = "UserRole.findAll", query = "SELECT f FROM UserRole f")
public class UserRole extends AbstractModel {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "USER_ROLE_ID_GENERATOR", sequenceName = "USER_ROLE_SEQ", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_ROLE_ID_GENERATOR")
	private Long id;

	// bi-directional many-to-one association to FrtRole
	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private Role role;

	// bi-directional many-to-one association to FrtProdEnduser
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "USER_ID")
	private Enduser enduser;

	public UserRole() {
	}

	@SuppressWarnings("unchecked")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Enduser getEnduser() {
		return enduser;
	}

	public void setEnduser(Enduser enduser) {
		this.enduser = enduser;
	}

}