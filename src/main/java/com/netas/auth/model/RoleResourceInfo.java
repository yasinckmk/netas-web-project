package com.netas.auth.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;



public class RoleResourceInfo implements Serializable, GrantedAuthority {
	private static final long serialVersionUID = 1L;

	private Role role;
	
	private boolean inherited;
	
	private List<Resource> resources =new ArrayList<Resource>();
	
	private List<Resource> revokedResources = new ArrayList<Resource>();
	
	private RoleSource source;
	
	public enum RoleSource {
		ORG,
		USER
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public boolean isInherited() {
		return inherited;
	}

	public void setInherited(boolean inherited) {
		this.inherited = inherited;
	}

	public List<Resource> getResources() {
		return resources;
	}

	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}

	public RoleSource getSource() {
		return source;
	}

	public void setSource(RoleSource source) {
		this.source = source;
	}

	@Override
	public String getAuthority() {
		return role !=null ? role.getName() : "NONE";
	}

	

	public boolean matches(String url)
	{
		boolean result = false;
		
		if(resources!=null && !resources.isEmpty())
		{
			for(Resource res : resources)
			{
				if(res.matches(url))
				{
					result = true;
					break;
				}
			}
		}
		
		return result;
	}

	public List<Resource> getRevokedResources() {
		return revokedResources;
	}

	public void setRevokedResources(List<Resource> revokedResources) {
		this.revokedResources = revokedResources;
	}
	
	
	@Override
	public String toString() {
		
		return role !=null ? role.toString(): StringUtils.EMPTY;
	}
}


