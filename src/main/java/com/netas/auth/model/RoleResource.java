package com.netas.auth.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.netas.db.model.AbstractModel;


/**
 * The persistent class for the A_ROLE_RESOURCE database table.
 * 
 */
@Entity
@Table(name="A_ROLE_RESOURCE")
@NamedQuery(name="RoleResource.findAll", query="SELECT f FROM RoleResource f")
public class RoleResource extends AbstractModel  {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ROLE_RESOURCE_ID_GENERATOR", sequenceName="ROLE_RESOURCE_SEQ",allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ROLE_RESOURCE_ID_GENERATOR")
	private Long id;

	@Column(name="STATUS")
	private String status;

	//bi-directional many-to-one association to FrtResource
	@ManyToOne
	@JoinColumn(name="RESOURCE_ID")
	private Resource resource;

	//bi-directional many-to-one association to FrtRole
	@ManyToOne
	@JoinColumn(name="ROLE_ID")
	private Role role;

	
	public RoleResource() {
	}

	@SuppressWarnings("unchecked")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	




}