package com.netas.auth.model;

import java.util.StringJoiner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.netas.db.model.AbstractModel;




@Entity
@Table(name = "ENDUSER")
@NamedQuery(name = "Enduser.findAll", query = "SELECT f FROM Enduser f")
public class Enduser extends AbstractModel {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 19)
	private Long id;

	@Column(name = "EMAIL", nullable = false, length = 255)
	private String email;

	@Column(name = "FIRST_NAME", length = 255)
	private String firstName;
	
	@Column(name = "PASSWORD", length = 255)
	private String password;

	@Column(name = "SECOND_NAME", length = 255)
	private String secondName;

	@Column(name = "LAST_NAME", length = 255)
	private String lastName;

	@Column(name = "USER_NAME", nullable = false, length = 255)
	private String userName;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ZPERNR")
	private Enduser zper;

	@Transient
	private String userType;

	public Enduser() {
	}

	@Transient
	public String getIdString() {
		return String.valueOf(getId());
	}

	@Transient
	public String getConverterId() {
		// int index = displayName.indexOf("(");
		// return displayName.substring(0, index).trim();
		return userName;
	}

	@Transient
	public String getNameSurname() {
		String result = firstName;

		if (secondName != null)
			result = result + " " + secondName;

		if (getLastName() != null)
			result = result + " " + getLastName();

		return result;
	}

	@Transient
	public String getNameSurnameId() {
		return new StringJoiner("").add(getNameSurname()).add(" (").add(getIdString()).add(")").toString();
	}

	@Transient
	public String getDistinctName() {
		String result = getNameSurname();

		result = result + " (" + getUserName() + ")";

		return result;
	}

	@SuppressWarnings("unchecked")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	public Enduser getZper() {
		return zper;
	}


	public void setZper(Enduser zper) {
		this.zper = zper;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}