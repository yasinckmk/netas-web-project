package com.netas.auth.model;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.netas.db.model.AbstractModel;


/**
 * The persistent class for the A_RESOURCE database table.
 * 
 */
@Entity
@Table(name="A_RESOURCE")
@NamedQuery(name="Resource.findAll", query="SELECT f FROM Resource f")
public class Resource extends AbstractModel {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RESOURCE_ID_GENERATOR", sequenceName="RESOURCE_SEQ", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RESOURCE_ID_GENERATOR")
	private Long id;

	private String description;

	private String name;

	@Column(name="URL_PATTERN")
	private String urlPattern;
	
	@Transient
	private Pattern pattern;
	

	//bi-directional many-to-one association to FrtRoleResource
	@OneToMany(mappedBy="resource", orphanRemoval=true)
	private Set<RoleResource> roleResources = new LinkedHashSet<RoleResource>();
	

	public Resource() {
	}

	@SuppressWarnings("unchecked")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void compile ()
	{
		if(StringUtils.isNotBlank(urlPattern))
		{
			pattern = Pattern.compile(urlPattern);
		}
	}

	public boolean matches (String url)
	{
		boolean res = false;		
		if (pattern != null) {
			try {
				Matcher matcher = pattern.matcher(url);
				res = matcher.matches();
			} catch (Exception e) {	}
		}
		
		return res;	
	}
	
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<RoleResource> getRoleResources() {
		return roleResources;
	}

	public void setRoleResources(Set<RoleResource> roleResources) {
		this.roleResources = roleResources;
	}

	
	@Override
	public String toString() {
		return name;
	}

	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	public String getUrlPattern() {
		return urlPattern;
	}

	public void setUrlPattern(String urlPattern) {
		this.urlPattern = urlPattern;
	}
}