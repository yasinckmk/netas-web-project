package com.netas.auth.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.netas.db.model.AbstractModel;

/**
 * The persistent class for the A_ROLE database table.
 * 
 */
@Entity
@Table(name = "A_ROLE")
@NamedQuery(name = "Role.findAll", query = "SELECT f FROM Role f")
public class Role extends AbstractModel {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ROLE_ID_GENERATOR", sequenceName = "ROLE_SEQ", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROLE_ID_GENERATOR")
	private Long id;

	private String description;

	private String name;

	// bi-directional many-to-one association to FrtUserRole
	@OneToMany(mappedBy = "role", orphanRemoval = true)
	private Set<UserRole> userRoles = new LinkedHashSet<UserRole>();

	// bi-directional many-to-one association to FrtRoleResource
	@OneToMany(mappedBy = "role", orphanRemoval = true)
	private Set<RoleResource> roleResources = new LinkedHashSet<RoleResource>();

	public Role() {
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public Set<RoleResource> getRoleResources() {
		return roleResources;
	}

	public void setRoleResources(Set<RoleResource> roleResources) {
		this.roleResources = roleResources;
	}

	@SuppressWarnings("unchecked")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return name;
	}
}