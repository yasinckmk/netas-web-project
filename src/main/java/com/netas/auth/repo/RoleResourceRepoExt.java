package com.netas.auth.repo;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Resource;
import com.netas.auth.model.Role;
import com.netas.auth.model.RoleResourceInfo;


@Transactional
public interface RoleResourceRepoExt {
	
	void addRoleResources(Role role, List<Resource> resources, String status);

	List<RoleResourceInfo> getRoleResourceInfos(long userId, long activeOrgId);

}
