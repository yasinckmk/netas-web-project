package com.netas.auth.repo;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
//import com.tk.emd.ui.util.CoreUtil;

import com.netas.auth.model.Enduser;
import com.netas.auth.model.Role;
import com.netas.auth.model.UserRole;


@Transactional
public class UserRoleRepoExtImpl extends AbstractExtRepo  implements UserRoleRepoExt {

	
	@Transactional
	public void addUserRoles(Role role, List<Enduser> userList) {
		// TODO Auto-generated method stub
		
		for(Enduser eu: userList)
		{
			UserRole userRole = new UserRole();
			userRole.setRole(role);
			userRole.setEnduser(eu);
			userRole.setCreateUser("Yasin");//CoreUtil.getLoginUserName()  Security context principal dan bu deger alınacak.
			userRole.setCreateDate(new Date());
			entityManager.persist(userRole);
		}
	}



}
