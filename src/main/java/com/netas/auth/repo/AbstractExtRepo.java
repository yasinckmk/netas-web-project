package com.netas.auth.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


public class AbstractExtRepo {
	
	protected static final Logger log = LoggerFactory.getLogger(AbstractExtRepo.class);
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	@Qualifier("netasDBTemp")
	@Autowired
	protected NamedParameterJdbcTemplate netasDBTemp;
	
}
