package com.netas.auth.repo;

import java.util.List;

import javax.inject.Named;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Role;


@Transactional
public interface RoleRepo extends JpaRepository<Role, Long> {
	
	
	@Query("from Role ro order by ro.name")
	List<Role> getRoles();
	
	

	
	@Query("select ro from Role ro "
			+ "	left join fetch ro.userRoles ur "
			+ "	left join fetch ur.enduser eu "
			+ "	left join fetch ro.roleResources rr "
			+ "	left join fetch rr.resource where ro.id = ?1 order by ro.name ")
	Role getRole(Long roleId);


	
	
}
