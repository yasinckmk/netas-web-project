package com.netas.auth.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Enduser;
import com.netas.auth.model.Role;
import com.netas.auth.model.RoleResource;
import com.netas.auth.model.RoleResourceInfo;

@Transactional
public interface RoleResourceRepo extends JpaRepository<RoleResource, Long>, RoleResourceRepoExt {

}
