package com.netas.auth.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Resource;


@Transactional
public interface ResourceRepo extends JpaRepository<Resource, Long>  {
	
	@Query("select res from Resource res  order by res.id")
	public List<Resource> getResources();
	
	
}
