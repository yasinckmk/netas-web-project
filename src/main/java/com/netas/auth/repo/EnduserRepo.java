package com.netas.auth.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Enduser;


@Transactional
public interface EnduserRepo extends JpaRepository<Enduser, Long> {
	

	
	@Query("select e from Enduser e where e.id=?1")
	public Enduser getEnduserBy (Long id);
	


	
}
