package com.netas.auth.repo;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Enduser;
import com.netas.auth.model.Role;


@Transactional
public interface UserRoleRepoExt {
	
	void addUserRoles(Role role,List<Enduser> userList);

}
