package com.netas.auth.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.UserRole;

@Transactional
public interface UserRoleRepo extends JpaRepository<UserRole, Long> , UserRoleRepoExt {
	
}
