package com.netas.auth.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Resource;
import com.netas.auth.model.Role;
//import com.tk.oms.ui.util.CoreUtil;
import com.netas.auth.model.RoleResource;
import com.netas.auth.model.RoleResourceInfo;
import com.netas.auth.model.RoleResourceInfo.RoleSource;


@Transactional
public class RoleResourceRepoImpl extends AbstractExtRepo implements RoleResourceRepoExt {
	
	@Autowired
	@Qualifier("queryUserRoleResources")
	String queryUserRoleResources;
	

	@Transactional
	@Override
	public void addRoleResources(Role role, List<Resource> resources ,String status) {


		for(Resource res: resources)
		{
			RoleResource roleResource = new RoleResource();
			roleResource.setRole(role);
			roleResource.setResource(res);
			roleResource.setStatus(status);
		//	roleResource.setCreateUser(CoreUtil.getLoginUserName());
			roleResource.setCreateDate(new Date());
			entityManager.persist(roleResource);
		}


	}
	
	
	 @Transactional
	 @Override
	 public List<RoleResourceInfo> getRoleResourceInfos (long userId , long activeOrgId)
	 {
			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put("USER_ID_PARAM", userId);
			//paramMap.put("ACTIVE_ORG", activeOrgId)
			//paramMap.put("ACTIVE_ORG_2", activeOrgId);
			List<RoleResourceInfo> resultList = netasDBTemp.query(queryUserRoleResources,paramMap,new RowMapper<RoleResourceInfo>() {
				@Override
				public RoleResourceInfo mapRow(ResultSet rs, int paramInt) throws SQLException {
					RoleResourceInfo inf = new RoleResourceInfo();

					Role role = new Role();
					Resource res = new Resource();
					
					role.setId(rs.getLong("ROLE_ID"));
					role.setName(rs.getString("ROLE_NAME"));
					role.setDescription(rs.getString("ROLE_DESCRIPTION"));
					inf.setRole(role);
					inf.setInherited(rs.getBoolean("INHERITED"));
					
					res.setId(rs.getLong("RES_ID"));
					
					if(res.getId() !=null)
					{
						String status = rs.getString("STATUS");
						
						res.setName(rs.getString("RES_NAME"));
						res.setDescription(rs.getString("RES_DESCRIPTION"));
						res.setUrlPattern(rs.getString("RES_URL"));						
						try{
							res.compile();
						}catch (Exception e)
						{
							//log.error("Cannot compile resource pattern, resource name: " +res.getName() + ", res url pattern: " + res.getUrlPattern() , e);
						}
						
						if("Given".equals(status))
						{
							inf.getResources().add(res);
						}else
						{
							inf.getRevokedResources().add(res);
						}
						
					}
					
					inf.setSource(RoleSource.valueOf(rs.getString("SOURCE")));
					
					return inf;
					
				}
			});
			
			
			Map<Long, RoleResourceInfo> roles = new LinkedHashMap<Long, RoleResourceInfo>();
			Set<Long> revokedResources = new HashSet<Long>();
			if(resultList !=null)
			{
				for(RoleResourceInfo info : resultList)
				{
					RoleResourceInfo existing = roles.get(info.getRole().getId());
					if(existing ==null)
					{
						roles.put(info.getRole().getId(), info);
						
					}else
					{
						existing.getResources().addAll(info.getResources());
						existing.getRevokedResources().addAll(info.getRevokedResources());
					}		
					
					if(!info.getRevokedResources().isEmpty())
					{
						revokedResources.add(info.getRevokedResources().get(0).getId());
					}
					
				}
			}
			
			resultList = new ArrayList<RoleResourceInfo>();
			resultList.addAll(roles.values());
			
			
			for(RoleResourceInfo info : resultList)
			{
				List<Resource> removed = new ArrayList<Resource>();
				for(Resource res : info.getResources())
				{
					if(revokedResources.contains(res.getId()))
					{
						removed.add(res);
					}
							
				}
				
				info.getResources().removeAll(removed);
				
			}
		
			
			return resultList;
	 }


}
