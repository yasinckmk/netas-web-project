package com.netas.auth.repo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Enduser;


@Transactional
public class EnduserRepoImpl  {

	@Autowired
	private EnduserRepo userRepo;

	@Transactional
	public List<Enduser> retrieveEndUsers() {

		List<Enduser> result = userRepo.findAll();
		return result;
	}

}
