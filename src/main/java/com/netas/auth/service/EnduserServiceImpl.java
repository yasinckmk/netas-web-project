package com.netas.auth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Enduser;
import com.netas.auth.model.RoleResourceInfo;
import com.netas.auth.repo.EnduserRepo;
import com.netas.auth.repo.RoleResourceRepo;


@Service("enduserService")
public class EnduserServiceImpl implements EnduserService {
	
	@Autowired
	private EnduserRepo enduserRepo;
	
	@Autowired
	private RoleResourceRepo roleResourceRepo;


	@Override
	public Enduser getEnduserById(Long id) {
			return enduserRepo.getEnduserBy(id);
	}


	@Override
	public List<Enduser> retrieveEndUsers() {
		return enduserRepo.findAll();
	}

	
	@Transactional
	@Override
	public List<RoleResourceInfo> getRoleResourceInfos(long userId, long activeOrgId) {

		return roleResourceRepo.getRoleResourceInfos(userId, activeOrgId);
	}
}