package com.netas.auth.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netas.auth.model.Enduser;
import com.netas.auth.model.Resource;
import com.netas.auth.model.Role;
import com.netas.auth.model.RoleResource;
import com.netas.auth.model.RoleResourceInfo;
import com.netas.auth.model.UserRole;
import com.netas.auth.repo.ResourceRepo;
import com.netas.auth.repo.RoleRepo;
import com.netas.auth.repo.RoleResourceRepo;
import com.netas.auth.repo.UserRoleRepo;


@Service
public class SecurityServiceImpl implements SecurityService{
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;
    
    @Autowired
	private RoleRepo roleRepo;

	@Autowired
	private RoleResourceRepo roleResourceRepo;

	@Autowired
	private UserRoleRepo userRoleRepo;

	@Autowired
	private ResourceRepo resourceRepo;
	
	

	public SecurityServiceImpl() {
		System.out.println("SecurityServiceImpl created");
	}

	@Override
	public List<Role> getRoles() {

		return roleRepo.getRoles();

	}

	@Transactional
	@Override
	public void addRole(Role newRole) {

		newRole.setCreateDate(new Date());
		// newRole.setCreateUser(CoreUtil);
		roleRepo.save(newRole);

	}
	
	
//	@Transactional
//	@Override
//	public void addUser(Enduser user) {
//
//		user.setCreateDate(new Date());
//		// newRole.setCreateUser(CoreUtil);
//		enduserRepo.save(user);
//
//	}

	@Transactional
	@Override
	public void deleteRole(Role role) {

		roleRepo.delete(role);
	}

	@Transactional
	@Override
	public Role getRole(Long id) {
		return roleRepo.getRole(id);
	}



	@Override
	public void updateRole(Role newRole) {

		newRole.setUpdateDate(new Date());
		newRole.setUpdateUser("");
		roleRepo.save(newRole);
	}
	
//	@Override
//	public void updateUser(Enduser newUser) {
//		newUser.setUpdateDate(new Date());
//		newUser.setUpdateUser("");
//		enduserRepo.save(newUser);
//	}


	@Transactional
	@Override
	public List<Resource> getResources() {

		return resourceRepo.getResources();

	}

	@Override
	public void deleteResource(Resource resource) {
		resourceRepo.delete(resource);
	}

	@Override
	public void addResource(Resource newResource) {

		newResource.setCreateDate(new Date());
		newResource.setCreateUser("");
		resourceRepo.save(newResource);

	}

	@Override
	public void updateResource(Resource newResource) {

		newResource.setUpdateDate(new Date());
		newResource.setUpdateUser("");
		resourceRepo.save(newResource);

	}

	@Override
	public void deleteRoleResource(RoleResource roleRes) {

		roleResourceRepo.delete(roleRes);

	}

	@Transactional
	@Override
	public void addRoleResources(Role role, List<Resource> resources, String status) {

		roleResourceRepo.addRoleResources(role, resources, status);

	}

	

	@Transactional
	@Override
	public void deleteUserRole(UserRole userRole) {
		userRoleRepo.delete(userRole);

	}

	@Transactional
	@Override
	public void addUserRoles(Role role, List<Enduser> userList) {
		userRoleRepo.addUserRoles(role, userList);

	}

    

    private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);

    @Override
    public String findLoggedInUsername() {
        Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (userDetails instanceof UserDetails) {
            return ((UserDetails)userDetails).getUsername();
        }

        return null;
    }

    @Override
    public void autoLogin(String username, String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            logger.debug(String.format("Auto login %s successfully!", username));
        }
    }
}
