package com.netas.auth.service;

import java.util.List;

import com.netas.auth.model.Enduser;
import com.netas.auth.model.RoleResourceInfo;

public interface EnduserService {

	List<Enduser> retrieveEndUsers();

	Enduser getEnduserById(Long id);

	List<RoleResourceInfo> getRoleResourceInfos(long userId, long activeOrgId);

}
