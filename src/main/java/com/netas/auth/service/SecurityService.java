package com.netas.auth.service;

import java.util.List;

import com.netas.auth.model.Enduser;
import com.netas.auth.model.Resource;
import com.netas.auth.model.Role;
import com.netas.auth.model.RoleResource;
import com.netas.auth.model.RoleResourceInfo;
import com.netas.auth.model.UserRole;

public interface SecurityService {
	public String findLoggedInUsername();

	public void autoLogin(String username, String password);

	public List<Role> getRoles();

	public void addRole(Role newRole);

	public void deleteRole(Role role);

	public Role getRole(Long id);

	public void deleteUserRole(UserRole userRole);

	public void addUserRoles(Role role, List<Enduser> userList);

	public void updateRole(Role newRole);

	public List<Resource> getResources();

	public void deleteResource(Resource resource);

	public void addResource(Resource newResource);

	public void updateResource(Resource newResource);

	public void deleteRoleResource(RoleResource roleRes);

	public void addRoleResources(Role role, List<Resource> resources, String status);


}
