package com.netas.auth.service;

import com.netas.auth.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
