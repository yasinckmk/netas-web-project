package com.netas.auth.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netas.db.model.Product;
import com.netas.web.services.ProductService;

@RestController
public class ProductController {
    @Autowired
    private ProductService productService;
    
    
    @GetMapping("/api/foos")
    @ResponseBody
    public String getFoos(@RequestParam String id) {
        return "ID: " + id;
    }

    @RequestMapping(value = "/productt", method = RequestMethod.GET)
    public List<Product> listProduct() {
    	System.out.println( productService.listAllProducts().size());
        return productService.listAllProducts();
    }

    @RequestMapping(value = "/productt", method = RequestMethod.POST)
    public Product create(@RequestBody Product product) {
        return productService.saveProduct(product);
    }

//    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
//    public String delete(@PathVariable(value = "id") Long id) {
//        productService.delete(id);
//        return "success";
//    }
	
}
