package com.netas.auth.ui.bean;

import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.netas.auth.model.Resource;
import com.netas.auth.service.SecurityService;


@Component("resourceBean")
@ViewScoped
public class ResourceBean  {

	@Autowired
	private SecurityService securityService;
	
	private boolean resourceDialogRendered = false;
		
	private List<Resource> modelList;

	private Resource newResource;
	
	
	@PostConstruct
	public void init()
	{
		reload();
	}
	
	public void reload()
	{
		resourceDialogRendered=false;
		modelList = securityService.getResources();
	}
	
	public void addNewResource()
	{
		resourceDialogRendered=true;
		newResource=new Resource();
	}
	
	public void edit(Resource resource)
	{
		resourceDialogRendered=true;
		newResource=resource;
	}
	
	public void delete(Resource resource)
	{
		securityService.deleteResource(resource);
		reload();
	}
	
	public void cancel()
	{
		resourceDialogRendered=false;
	}
	
	public void saveNewResource()
	{
		if(StringUtils.isNotBlank(newResource.getUrlPattern()))
		{
			try{
				Pattern.compile(newResource.getUrlPattern());
				
			}catch(Throwable e)
			{
				//MessageUtils.addFacesMessageByKey(FacesMessage.SEVERITY_ERROR, "general.message.invalidValue", MessageUtils.getMessage("resource.label.urlPattern"));	
				return;
			}
		}
		
		if(newResource.getId()==null)
			securityService.addResource(newResource);
		else
			securityService.updateResource(newResource);
		reload();
			
	}

	public boolean isResourceDialogRendered() {
		return resourceDialogRendered;
	}

	public void setResourceDialogRendered(boolean resourceDialogRendered) {
		this.resourceDialogRendered = resourceDialogRendered;
	}

	public List<Resource> getModelList() {
		return modelList;
	}

	public void setModelList(List<Resource> modelList) {
		this.modelList = modelList;
	}

	public Resource getNewResource() {
		return newResource;
	}

	public void setNewResource(Resource newResource) {
		this.newResource = newResource;
	}
	
}
