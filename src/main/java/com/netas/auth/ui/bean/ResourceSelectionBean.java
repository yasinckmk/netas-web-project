package com.netas.auth.ui.bean;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.ToggleSelectEvent;

import com.netas.auth.model.Resource;




public class ResourceSelectionBean  {

	private String id;
	
	private List<Resource> resourceList;

	private List<Resource> selectedResources;
	
	public ResourceSelectionBean(String id)
	{
		this.id=id; 
	}
	
	
	public void rowsSelected(AjaxBehaviorEvent event)
    {
    	ToggleSelectEvent e = (ToggleSelectEvent)event;
   
    	 DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(id);
    	 
    	if( e.isSelected())
    	{
    		dataTable.setSelection(resourceList);
    		
    	}else
    	{
    		dataTable.setSelection(null);
    	}
    	
    }


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public List<Resource> getResourceList() {
		return resourceList;
	}


	public void setResourceList(List<Resource> resourceList) {
		this.resourceList = resourceList;
	}


	public List<Resource> getSelectedResources() {
		return selectedResources;
	}


	public void setSelectedResources(List<Resource> selectedResources) {
		this.selectedResources = selectedResources;
	}
	
	
}
