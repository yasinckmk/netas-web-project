package com.netas.auth.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netas.auth.model.Enduser;
import com.netas.auth.model.Resource;
import com.netas.auth.model.Role;
import com.netas.auth.model.RoleResource;
import com.netas.auth.model.UserRole;
import com.netas.auth.service.SecurityService;


@ViewScoped
@Component("roleBean")
public class RoleBean implements UserSelectionHolder {

	@Autowired
	private SecurityService securityService;

//	@Autowired
//	private EnduserService enduserService;

	private List<Role> modelList;

	private Role selectedModel;

	private Role newRole = new Role();
	private Enduser newUser = new Enduser();

	private boolean roleDialogRendered;
	private boolean userDialogRendered;

	private boolean resourceDialogRendered;

	private boolean orgSelectionRendered;

	private boolean userSelectionRendered;

	private Set<UserRole> userRoles;

	private boolean orgRoleInheritable = true;

	private UserSelectionBean userSelectionBean;

	private String resourceStatus;

	private Set<RoleResource> roleResources;

	private ResourceSelectionBean resourceSelectionBean;

	@PostConstruct
	public void init() {
		reload();
	}

	public void reload() {
		modelList = securityService.getRoles();
		newRole = new Role();
		newUser= new Enduser();
		roleDialogRendered = false;
		setUserDialogRendered(false);
		resourceDialogRendered = false;
		orgSelectionRendered = false;
		userSelectionRendered = false;
		orgRoleInheritable = false;
		orgRoleInheritable = true;
		resourceStatus = null;

		if (selectedModel != null) {
			for (Role role : modelList) {
				if (selectedModel.getId().equals(role.getId())) {
					selectedModel = role;
					roleRowSelected();
					break;
				}
			}
		}

	}

	public void roleRowSelected() {
		Role role = securityService.getRole(selectedModel.getId());
		userRoles = role.getUserRoles();
		roleResources = role.getRoleResources();
		DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("resourceListForm:modelList");
		dataTable.reset();
		dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("userListForm:userList");
		dataTable.reset();

	}

	public void delete(Role role) {
		securityService.deleteRole(role);
		reload();
	}

	public void edit(Role role) {
		roleDialogRendered = true;
		newRole = role;

	}

	public void addNewRole() {
		newRole = new Role();
		roleDialogRendered = true;
	}

	public void saveNewRole() {
		try {
			if (newRole.getId() == null)
				securityService.addRole(newRole);
			else
				securityService.updateRole(newRole);
			reload();
		} catch (Exception e) {
			//MessageUtils.addCommonExceptionMessage(e);
		}
	}

	public void cancel() {
		roleDialogRendered = false;
	}

	public void saveNewUsers() {
		List<Enduser> userList=new ArrayList<Enduser>();
		userList.add(getNewUser());
		try {
			selectedModel.setRoleResources(roleResources);
			securityService.addUserRoles(selectedModel, userList);
			reload();
		} catch (Exception e) {
			//MessageUtils.addCommonExceptionMessage(e);
		}
	}

	public void addNewUser() {
		setNewUser(new Enduser());
		userSelectionRendered = true;
		setUserDialogRendered(true);
	}

	public void cancelNewOrgs() {
		orgSelectionRendered = false;
	}

	public void cancelNewUsers() {
		setUserDialogRendered(false);
	}

	public void cancelNewResources() {
		resourceDialogRendered = false;
	}

	public void deleteUser(UserRole userRole) {
		securityService.deleteUserRole(userRole);
		reload();
	}

	public List<Role> getModelList() {
		return modelList;
	}

	public void setModelList(List<Role> modelList) {
		this.modelList = modelList;
	}

	public Role getSelectedModel() {
		return selectedModel;
	}

	public void setSelectedModel(Role selectedModel) {
		this.selectedModel = selectedModel;
	}

	public Role getNewRole() {
		return newRole;
	}

	public void setNewRole(Role newRole) {
		this.newRole = newRole;
	}

	public boolean isRoleDialogRendered() {
		return roleDialogRendered;
	}

	public void setRoleDialogRendered(boolean roleDialogRendered) {
		this.roleDialogRendered = roleDialogRendered;
	}

	public Set<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public boolean isOrgSelectionRendered() {
		return orgSelectionRendered;
	}

	public void setOrgSelectionRendered(boolean orgSelectionRendered) {
		this.orgSelectionRendered = orgSelectionRendered;
	}

	public boolean isOrgRoleInheritable() {
		return orgRoleInheritable;
	}

	public void setOrgRoleInheritable(boolean orgRoleInheritable) {
		this.orgRoleInheritable = orgRoleInheritable;
	}

	public boolean isUserSelectionRendered() {
		return userSelectionRendered;
	}

	public void setUserSelectionRendered(boolean userSelectionRendered) {
		this.userSelectionRendered = userSelectionRendered;
	}

	public boolean isResourceDialogRendered() {
		return resourceDialogRendered;
	}

	public void setResourceDialogRendered(boolean resourceDialogRendered) {
		this.resourceDialogRendered = resourceDialogRendered;
	}

	public String getResourceStatus() {
		return resourceStatus;
	}

	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	public Set<RoleResource> getRoleResources() {
		return roleResources;
	}

	public void setRoleResources(Set<RoleResource> roleResources) {
		this.roleResources = roleResources;
	}

	public void addNewResource() {
		setResourceSelectionBean(new ResourceSelectionBean(""));
		getResourceSelectionBean().setResourceList(securityService.getResources());
		resourceDialogRendered = true;
	}

	public ResourceSelectionBean getResourceSelectionBean() {
		return resourceSelectionBean;
	}

	public void setResourceSelectionBean(ResourceSelectionBean resourceSelectionBean) {
		this.resourceSelectionBean = resourceSelectionBean;
	}

	public void saveNewResources() {
		List<Resource> resources = resourceSelectionBean.getSelectedResources();
		try {
			securityService.addRoleResources(selectedModel, resources, resourceStatus);
			reload();
		} catch (Exception e) {
			//MessageUtils.addCommonExceptionMessage(e);
		}

	}

	public UserSelectionBean getUserSelectionBean() {
		return userSelectionBean;
	}

	public void setUserSelectionBean(UserSelectionBean userSelectionBean) {
		this.userSelectionBean = userSelectionBean;
	}

	public boolean isUserDialogRendered() {
		return userDialogRendered;
	}

	public void setUserDialogRendered(boolean userDialogRendered) {
		this.userDialogRendered = userDialogRendered;
	}

	public Enduser getNewUser() {
		return newUser;
	}

	public void setNewUser(Enduser newUser) {
		this.newUser = newUser;
	}

}
