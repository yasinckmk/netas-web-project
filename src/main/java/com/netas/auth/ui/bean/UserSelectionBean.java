package com.netas.auth.ui.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;

import com.netas.auth.model.Enduser;


public class UserSelectionBean {

	private String id;

	private boolean readOnly = false;

	private boolean rendered = true;

	private boolean multiple = false;

	private List<Enduser> selectedUsers = new ArrayList<Enduser>();

	private List<Enduser> filteredUsers = new ArrayList<Enduser>();

	private List<Enduser> usersList = new ArrayList<Enduser>();

	private Enduser selectedUser;

	private int sizeLimit = 1000;

	 private UserSelectionHolder parent;

	public UserSelectionBean(String id,UserSelectionHolder parent ) {
		this.id = id;
		this.parent=parent;
	}

	public void clear() {
		selectedUsers.clear();
		usersList.clear();
		selectedUser = null;
		filteredUsers.clear();
	}

	public void setUsersList(Collection<Enduser> list) {
		clear();

		if (list != null) {
			if (list.size() > sizeLimit)
				// MessageUtils.addFacesMessageByMessage(FacesMessage.SEVERITY_WARN,MessageUtils.getMessage("fareRequest.warning.userCountLimit",list.size(),
				 //sizeLimit));
				usersList.addAll(list);
			filteredUsers.addAll(list);
		}

	}

	public void userRowsSelected(AjaxBehaviorEvent event) {
		ToggleSelectEvent e = (ToggleSelectEvent) event;

		DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(id);

		if (e.isSelected()) {
			dataTable.setSelection(usersList);

		} else {
			dataTable.setSelection(null);
		}

	}

	public void userRowSelected(SelectEvent event) {
		SelectEvent e = (SelectEvent) event;

		selectedUser = (Enduser) e.getObject();

	}

	public boolean isMultiple() {
		return multiple;
	}

	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Enduser getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(Enduser selectedUser) {
		this.selectedUser = selectedUser;
	}

	public List<Enduser> getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(List<Enduser> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	public List<Enduser> getUsersList() {
		return usersList;
	}

	public int getSizeLimit() {
		return sizeLimit;
	}

	public void setSizeLimit(int sizeLimit) {
		this.sizeLimit = sizeLimit;
	}

	public boolean isRendered() {
		return rendered;
	}

	public void setRendered(boolean rendered) {
		this.rendered = rendered;
	}

	public List<Enduser> getFilteredUsers() {
		return filteredUsers;
	}

	public void setFilteredUsers(List<Enduser> filteredUsers) {
		this.filteredUsers = filteredUsers;
	}
	
	public UserSelectionHolder getParent() {
		return parent;
	}


	public void setParent(UserSelectionHolder parent) {
		this.parent = parent;
	}

}
