package com.netas.auth.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.netas.security.OneRoleVoter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	@Qualifier("userDetailsServiceImpl")
	private UserDetailsService userDetailsService;

//	@Autowired
//	private OneRoleVoter oneRoleVoter;

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/javax.faces.*/**", "/img/**", "/css/**", "/js/**","/xhtml/**", "/webservice/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// How to Solve 403 Error in Spring Boot Post Request
	http.cors().and().csrf().disable();
//
//		http.authorizeRequests()
//				.and().formLogin().loginPage("/login") // IS_AUTHENTICATED_FULLY
//				.and().exceptionHandling().accessDeniedPage("/accessDenied.xhtml").and().headers().frameOptions()
//				.sameOrigin();

		http.httpBasic().disable();
		
//		http.authorizeRequests()
//	  	.antMatchers("/", "/home").permitAll()
//	  	.antMatchers("/admin/**").access("hasRole('ADMIN')")
//	  	.antMatchers("/db/**").access("hasRole('ADMIN') and hasRole('DBA')")
//	  	.and().formLogin().loginPage("/login")
//	  	.usernameParameter("username").passwordParameter("password")
//	  	.and().csrf()
//	  	.and().exceptionHandling().accessDeniedPage("/Access_Denied");
//		
		
	//	http.authorizeRequests().antMatchers("/resources/**", "/registration", "/h2", "/console/**", "/h2-console/**")
		//		.permitAll().anyRequest().authenticated().and().formLogin().loginPage("/login").permitAll();

//		
//		
		 http.authorizeRequests()
		    .antMatchers("/", "/index2").permitAll()
	        .antMatchers("/role/**").access("hasRole('ROLE_ADMIN')")
	        .antMatchers("/resource/**").access("hasRole('ROLE_GUEST') or hasRole('ROLE_ADMIN')")
	        .and().formLogin().loginPage("/login")
					.usernameParameter(
							"username").passwordParameter("password")
	        .and().exceptionHandling().accessDeniedPage("/Access_Denied");	


//	      
//	      http
//			.authorizeRequests()
//			.accessDecisionManager(accessDecisionManager())
//				.antMatchers("/accessDenied.xhtml", "/index.html").access("IS_AUTHENTICATED_ANONYMOUSLY")          
//				.antMatchers("/**").access("IS_AUTHENTICATED_FULLY") //IS_AUTHENTICATED_FULLY
//			.and().exceptionHandling().accessDeniedPage("/accessDenied.xhtml")
//			.and().formLogin().loginPage("/login").permitAll().and()
//			.logout().permitAll();

		http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
	}

	@Bean
	public AuthenticationManager customAuthenticationManager() throws Exception {
		return authenticationManager();
	}

	// sayfalar arasinda tokenlanmis password bilgisi var
//	@Autowired
//	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
//	}

//	@Bean
//	public UnanimousBased accessDecisionManager() {
//		UnanimousBased adm = new UnanimousBased(Collections.singletonList(oneRoleVoter));
//		adm.setAllowIfAllAbstainDecisions(false);
//		return adm;
//	}
}