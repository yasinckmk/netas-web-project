//package com.netas.auth.config;
//
//
//import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
//
//import com.netas.auth.config.WebSecurityConfig;
//
//public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
//
//	@Override
//	protected Class<?>[] getRootConfigClasses() {
//		return new Class[] { WebSecurityConfig.class };
//	}
// 
//	@Override
//	protected Class<?>[] getServletConfigClasses() {
//		return null;
//	}
// 
//	@Override
//	protected String[] getServletMappings() {
//		return new String[] { "/" };
//	}
//
//}
//
