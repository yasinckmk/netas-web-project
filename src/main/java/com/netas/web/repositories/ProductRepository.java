package com.netas.web.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.netas.db.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{
}
