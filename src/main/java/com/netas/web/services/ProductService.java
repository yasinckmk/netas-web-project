package com.netas.web.services;

import java.util.List;

import com.netas.db.model.Product;

public interface ProductService {
    List<Product> listAllProducts();

    Product getProductById(Integer id);

    Product saveProduct(Product product);
}
