package com.netas.ui.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.netas.db.model.Product;
import com.netas.web.services.ProductServiceImpl;

@Component(value = "productBean")
@Scope("view")
public class ProductBean extends CrudBean<Product> implements Serializable {

	private static final long serialVersionUID = 1L;
	@Autowired
	private ProductServiceImpl productService;

	private List<Product> products;
	
	
	@PostConstruct
	public void init() {
		products=productService.listAllProducts();
	    selected = createNew();
	}
	
	@Override
	public Product createNew() {
		Product obj = new Product();
		return obj;
	}

	@Override
	public boolean validate(Product t) {
		
		return true;
	}

	@Override
	public void onSelect(Product t) {
		selected = (Product)t ;

	}

	@Override
	public boolean prepare(Product t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAction() {
		productService.saveProduct(selected);
	
		return true;
	}

	@Override
	public boolean updateAction() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateAction(Product t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void resetAction() {
		System.out.println("reset");
		setCrudMode(CrudMode.ADD);
		selected = createNew();
		
	}

	@Override
	public boolean deleteAction() {
		// TODO Auto-generated method stub
		return false;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

}
